import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

Vue.config.productionTip = false

window.shared_data = {
  jeuxvideo: [
    {
      id: 0,
      nom: 'Hotline Miami',
      developpeur: 0,
      annee: 2012,
      url: 'https://media.senscritique.com/media/000018438057/source_big/Hotline_Miami.png',
      description: "L'intrigue se déroule en 1989 à Miami. Un personnage inconnu, mais que les fans nomment Jacket en référence à son blouson, reçoit des appels qui lui ordonnent de commettre des crimes contre la mafia russe locale.",
      genre: "Action-Aventure",
      note: "5"
    },
    {
      id: 1,
      nom: 'Infamous 2',
      developpeur: 1,
      annee: 2009,
      url: 'https://www.cdiscount.com/pdt2/7/8/3/1/700x700/0711719174783/rw/infamous-2-jeu-console-ps3.jpg',
      description: "À la suite de la destruction d'Empire City, Cole se tourne alors vers New Marais à la recherche de puissance pour vaincre la Bête.",
      genre: "Action-Aventure",
      note: "4"
    },
    {
      id: 2,
      nom: 'Borderlands 2',
      developpeur: 2,
      annee: 2012,
      url: 'https://s3.gaming-cdn.com/images/products/14/271x377/borderlands-2-cover.jpg',
      description: "L’histoire commence cinq ans après la fin du tout premier Borderlands, où une nouvelle Arche bien plus grande est découverte dans les tréfonds de la planète de Pandore.",
      genre: "Jeu de tir",
      note: "5"
    },
    {
      id: 3,
      nom: 'Borderlands 3',
      developpeur: 2,
      annee: 2019,
      url: 'https://s2.gaming-cdn.com/images/products/709/271x377/709-cover.jpg',
      description: "Comme les précédents opus de la franchise, Borderlands 3 propose quatre personnages jouables, dits « chasseurs de l'arche ».",
      genre: "Jeu de tir",
      note: "3"
    },
    {
      id: 4,
      nom: 'Portal 2',
      developpeur: 3,
      annee: 2011,
      url: 'https://s2.gaming-cdn.com/images/products/220/271x377/portal-2-cover.jpg',
      description: "À la fin du premier opus, la protagoniste Chell détruit GLaDOS et échappe momentanément à l'établissement. Mais elle y est reconduite par une forme invisible à la voix robotique, identifiée plus tard par le scénariste Erik Wolpaw comme le « Robot Escorteur ».",
      genre: "Réflexion",
      note: "4"
    },
    {
      id: 5,
      nom: 'Ace Attorney',
      developpeur: 4,
      annee: 2001,
      url: 'https://gpstatic.com/acache/39/88/1/fr/packshot-afe5b762b48ddd7fdcc3858046b78d88.jpg',
      description: "Le joueur prendra le contrôle d'un avocat de la défense, devant innocenter son client, accusé d'avoir commis un meurtre. Durant les phases d'enquête, il faudra récolter des preuves, et obtenir des informations avec les différents témoins.",
      genre: "Réflexion",
      note: "4"
    },
    {
      id: 6,
      nom: 'Burnout Paradise',
      developpeur: 5,
      annee: 2008,
      url: 'https://www.cdiscount.com/pdt2/0/3/4/1/700x700/5030931055034/rw/burnout-paradise.jpg',
      description: "La jouabilité de Paradise est lancée dans la ville fictionnelle nommée « Paradise City », un monde ouvert dans lequel les joueurs courent dans de nombreux types de courses. Les joueurs peuvent également courir en-ligne.",
      genre: "Course",
      note: "4"
    },
    {
      id: 7,
      nom: 'Mirror\'s Edge',
      developpeur: 6,
      annee: 2008,
      url: 'http://image.jeuxvideo.com/images-sm/pc/m/e/medgpc0f.jpg',
      description: "Le joueur incarne Faith, une des meilleures Messagères de la ville. Elle transporte illégalement des messages mais comme la rue est trop dangereuse pour elle, elle passe par les toits ce qui nécessite de nombreuses et dangereuses acrobaties. Depuis quelque temps la police, auparavant très occupée, semble pourchasser beaucoup plus souvent les Messagers.",
      genre: "Action-Aventure",
      note: "5"
    },
    {
      id: 8,
      nom: 'PAYDAY 2',
      developpeur: 7,
      annee: 2013,
      url: 'https://s2.gaming-cdn.com/images/products/223/orig/payday-2-cover.jpg',
      description: "Payday 2 reprend 4 personnages du premier opus : Dallas, Chains, Wolf et Hoxton. Ces personnalités sont incarnées par les joueurs lors des parties, mais ne définissent pas la (ou les) spécialisation de chaque joueur. Chaque joueur peut personnaliser son style de jeu, en achetant des compétences, des armes et leurs modifications, ainsi que des masques et leurs modifications au fur et à mesure qu'il gagne des niveaux (de la réputation) et de l'argent.",
      genre: "Jeu de tir",
      note: "4"
    },
    {
      id: 9,
      nom: 'The Last of Us',
      developpeur: 8,
      annee: 2013,
      url: 'http://image.jeuxvideo.com/images/jaquettes/00052332/jaquette-the-last-of-us-remastered-playstation-4-ps4-cover-avant-g-1397063678.jpg',
      description: "Le titre prend place dans un univers post-apocalyptique après une pandémie provoquée par un champignon appelé le cordyceps. Les deux personnages principaux se nomment Joel (personnage incarné par le joueur) et Ellie, et doivent survivre ensemble lors de leur traversée du territoire américain.",
      genre: "Action-Aventure",
      note: "5"
    },
    {
      id: 10,
      nom: 'Yakuza 0',
      developpeur: 9,
      annee: 2017,
      url: 'https://gpstatic.com/acache/36/84/1/fr/packshot-b913ee8d6f74375eb1cc6b25eff14f42.jpg',
      description: "Kazuma Kiryu est accusé d'un crime dont il est parfaitement innocent, il doit donc se blanchir de toute accusation. De son côté Gorô Majima, responsable d'un cabaret lucratif, fait face à un dilemme : tuer quelqu'un et retrouver son rang au sein des yakuzas ou faire fructifier son commerce.",
      genre: "Action-Aventure",
      note: "4"
    },
  ],
  genres:[
    "Action-Aventure",
    "Jeu de tir",
    "Course",
    "Sport",
    "Musical",
    "Réflexion",
    "Autre",
  ],
  notes:[
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
  ],
  developpeurs:[
    {
      id: 0,
      nom: "Dennaton Games",
      nationalite: "Suédois",
      creation: 2012,
    },
    {
      id: 1,
      nom: "Sucker Punch Productions",
      nationalite: "Américain",
      creation: 1997,
    },
    {
      id: 2,
      nom: "Gearbox Software",
      nationalite: "Américain",
      creation: 1999,
    },
    {
      id: 3,
      nom: "Valve Corporation",
      nationalite: "Américain",
      creation: 1998,
    },
    {
      id: 4,
      nom: "Capcom Co., Ltd.",
      nationalite: "Japonais",
      creation: 1983,
    },
    {
      id: 5,
      nom: "Criterion Games",
      nationalite: "Anglais",
      creation: 1993,
    },
    {
      id: 6,
      nom: "DICE",
      nationalite: "Suédois",
      creation: 1992,
    },
    {
      id: 7,
      nom: "Overkill Software",
      nationalite: "Suédois",
      creation: 2009,
    },
    {
      id: 8,
      nom: "Naughty Dog",
      nationalite: "Américain",
      creation: 1984,
    },
    {
      id: 9,
      nom: "Ryu Ga Gotoku Studio",
      nationalite: "Japonais",
      creation: 2011,
    },
  ]
}

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
